# Python Syntax Highlighter

Este programa genera un archivo HTML de un resaltador de sintaxis para un código fuente del lenguaje de programación Python.

## Installation

1. Ejecuta la siguiente linea en una linea de comandos dentro de la misma carpeta en la que se encuentra el archivo **"syntax"**. Puedes probar con los archivos en la carpeta prueba agregando **"pruebas/"** antes de escribir el nombre del archivo. **NO INCLUYAS LAS COMILLAS AL ESCRIBIR EL NOMBRE DEL ARCHIVO.** 


```bash
escript syntax "insertatuarchivopythonaqui.py"
```
2. El comando anterior generará un archivo **"code.HTML"** dentro de la carpeta actual, al darle doble click podrás ver el resultado del resaltador de sintaxis en tu navegador favorito. **NO MUEVAS EL ARCHIVO HTML A OTRA CARPETA** , pues se perderán los estilos del archivo.

## Creador
Carlos Eduardo Ruiz Lira - A01735706

