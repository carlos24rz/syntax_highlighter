defmodule Syntax do
    # Codigo que genera un archivo html de resaltador de sintaxis para python
    def main(file \\ {}) do
     {:ok, contents} = File.read(file) ## Leer archivo python
     tokens = contents |> String.split("\n")  ## Separar texto por lineas
      |> Enum.map(fn x-> String.to_charlist(x) end) ## Convertir entrada a charlists
      |> Enum.map(fn x-> :lexer.string(x) end) ## Realizar analisis lexicografico
      |> Enum.map(fn {_,x,_}-> x end) ## Considerar solo el token y tokenchars

    ## Dar formato html a tokens
    tags = Enum.map(tokens, fn x ->
      Enum.map(x, fn {token,tchars} ->
        case token do
          :keyword -> "<span class=\"keyword\">#{tchars}</span>"
          :escape -> "#{tchars}"
          :name -> "<span class=\"name\">#{tchars}</span>"
          :builtfunction -> "<span class=\"builtfunction\">#{tchars}</span>"
          :builterr -> "<span class=\"builterr\">#{tchars}</span>"
          :strbyliteral -> "<span class=\"strbyliteral\">#{tchars}</span>"
          :string -> "<span class=\"string\">#{tchars}</span>"
          :int -> "<span class=\"int\">#{tchars}</span>"
          :float -> "<span class=\"float\">#{tchars}</span>"
          :delimiter -> "<span class=\"delimiter\">#{tchars}</span>"
          :imagnumber -> "<span class=\"imagnumber\">#{tchars}</span>"
          :operator -> "<span class=\"operator\">#{tchars}</span>"
          :comment -> "<span class=\"comment\">#{tchars}</span>"
          :type -> "<span class=\"type\">#{tchars}</span>"
          :char -> "<span class=\"char\">#{tchars}</span>"
          _ -> "<span class=\"foo\">#{tchars}</span>"
        end
      end)
    end)

    # Juntar tags en una sola linea para cada analisis lexicografico
    code = Enum.map(tags, fn x ->
      Enum.reduce(x, "", fn str, acc -> acc <> str end)
    end) |> Enum.join("\n")

    # Generar archivo html
    topHTML = "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n    <meta charset=\"UTF-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <link rel=\"stylesheet\" href=\"elixir/styles/style.css\" />\n    <meta name=\"viewport\" content=\"width=device-width, initinal-scale=1.0\">\n    <title>Document</title>\n</head>\n<body>\n<pre>\n    <code>\n"
    bottomHTML = "\n    </code>\n</pre>\n</body>\n</html>"
    codigo = topHTML <> code <> bottomHTML
    state = File.write("code.html",codigo)
    IO.puts(state)

  end
end
