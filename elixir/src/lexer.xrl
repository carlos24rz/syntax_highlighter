Definitions.mix

%Definicion de conjuntos esenciales para reglas
L = [1-9a-zA-Z|_|\s]
OPERATOR = \+|\-|\*|\*\*|/|//|@|<<|>>|&|\||\^|~|:=|<|>|<=|>=|==|!=|%
DELIMITERS = \(|\)|\[|\]|\{|\}|\,|\:|\.|;|=|\->|\+=|\-=|\*=|/=|//=|@=|&=|\|=|\^=||>>=|<<=|\*\*=|%=
COMMENT = #
WHITESPACE = [\t\n\r]

% Definicion de palabras reservadas del sistema
KEYWORD = False|None|True|and|as|assert|async|await|break|class|continue|def|del|elif|else|except|finally|for|from|global|if|import|in|is|lambda|nonlocal|not|or|pass|raise|return|try|while|with|yield
BUILTINTYPES = int|float|complex|list|tuple|range|str|bytes|bytearray|memoryview|set|frozenset|dict
BUILTINFUNCTIONS = abs|aiter|all|any|anext|ascii|bin|bool|breakpoint|callable|chr|classmethod|compile|delattr|dir|divmod|enumerate|eval|exec|filter|format|frozenset|getattr|globals|hasattr|hash|help|id|input|isinstance|issubclass|iter|len|locals|map|max|map|memoryview|min|next|object|oct|hex|open|ord|pow|print|property|range|repr|reversed|round|setattr|slice|sorted|staticmethod|str|sum|super|type|vars|zip
BUILTINERR = AssertionError|AttributeError|EOFError|FloatingPointError|GeneratorExit|ImportError|ModuleNotFoundError|IndexError|KeyError|KeyboardInterrupt|MemoryError|NameError|NotImplementedError|OSError|OverflowError|RecursionError|ReferenceError|RuntimeError|StopIteration|StopAsyncIteration|SyntaxError|IndentationError|TabError|SystemError|SystemExit|TypeError|UnboundLocalError|UnicodeError|UnicodeEncodeError|UnicodeDecodeError|UnicodeTranslateError|ValueError|ZeroDivisionError 

% Definicion de literales string y byte
ESCAPESEQ = \newline|\\|\a|\b|\f|\n|\r|\t|\v|\ooo
STRINGPREFIX = r|u|R|U|f|F|fr|Fr|fR|FR|rf|rF|Rf|RF
BYTEPREFIX = b|B|br|Br|bR|BR|rb|rB|Rb|RB
STRING = "([^\"\\]|\\.)*"
STRINGLITERAL = ({STRINGPREFIX}){STRING}
BYTELITERAL = {BYTEPREFIX}{STRING}+

% Definicion de literales entero
NONZERODIGIT = [1-9]
DIGIT = 0|1|2|3|4|5|6|7|8|9
BINDIGIT = 0|1
OCTDIGIT = 0|1|2|3|4|5|6|7
HEXDOWNCASE = a|b|c|d|e|f
HEXUPPERCASE = A|B|C|D|E|F
HEXDIGIT = {DIGIT}|{HEXDOWNCASE}|{HEXUPPERCASE}
BININTEGER = 0(b|B)[_{BINDIGIT}]+
OCTINTEGER = 0(o|O)[_{OCTDIGIT}]+
HEXINTEGER = 0(x|X)[_{HEXDIGIT}]+
DECINTEGER = {NONZERODIGIT}[_{DIGIT}]*|0+[_0]*
INTEGER = {DECINTEGER}|{BININTEGER}|{OCTINTEGER}|{HEXINTEGER}

% Definicion de literales flotantes
DIGITPART = [{DIGIT}|_?]+
FRACTION = \.{DIGITPART}
EXPONENT = (e|E)[\+|\-]?{DIGITPART}
POINTFLOAT = {DIGITPART}?{FRACTION}|{DIGITPART}\.
EXPONENTFLOAT = ({DIGITPART}|{POINTFLOAT}){EXPONENT}
FLOATNUMBER = {POINTFLOAT}|{EXPONENTFLOAT}

%Definicion de literales imaginarios
IMAGNUMBER = ({FLOATNUMBER}|{DIGITPART})(j|J)

Rules.
{WHITESPACE}+ : skip_token.
\s        : {token,{escape,TokenChars}}.
{KEYWORD}+ : {token,{keyword,TokenChars}}.
{BUILTINTYPES}+ : {token,{type,TokenChars}}.
'{L}+' : {token,{char,TokenChars}}.
{BUILTINFUNCTIONS}+ : {token,{builtfunction,TokenChars}}.
{BUILTINERR}+ : {token,{builterr,TokenChars}}.
{COMMENT}.+ : {token,{comment,TokenChars}}.
{STRINGLITERAL} : {token,{strbyliteral, TokenChars}}.
{STRING} : {token,{string, TokenChars}}.
{INTEGER} : {token,{int,TokenChars}}.
{FLOATNUMBER} : {token,{float,TokenChars}}.
{IMAGNUMBER} : {token,{imagnumber,TokenChars}}.
{OPERATOR} : {token,{operator,TokenChars}}.
{DELIMITERS} : {token,{delimiter,TokenChars}}.
{L}+       : {token,{name,TokenChars}}.






Erlang code.
